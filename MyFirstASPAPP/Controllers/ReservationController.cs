﻿using MyFirstASPAPP.Models;
using MyFirstASPAPP.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace MyFirstASPAPP.Controllers
{
    public class ReservationController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            // affiche le formulaire
            return View();
        }

        [HttpPost]
        public ActionResult Index(Reservation r)
        {
            // valide le formulaire
            if(ModelState.IsValid)
            {
                // Gmail
                // host = smtp.gmail.com
                // port = 465 ou 587
                // Si Valid => Envoi Email

                SmtpClient c = ServicesLocator.GetMailer();

                string body = $"<h1>Reservation au nom de {r.NomClient}</h1>";
                body += $"<p>Email: {r.EmailClient}</p>";
                body += $"<p>Date: {r.Date.ToShortDateString()}</p>";
                body += $"<p>Heure: {r.Time.ToShortTimeString()}</p>";

                MailMessage m = new MailMessage();
                m.From = new MailAddress("tb.pizzayolo2020@gmail.com");
                m.To.Add("tb.pizzayolo2020@gmail.com");
                m.Body = body;
                m.Subject = "Demande de Réservation";
                m.IsBodyHtml = true;

                c.Send(m);
                return RedirectToAction("Index", "Biere");
            }
            else
            {
                // Si Non valide =>
                // afficher message d'erreur
                ViewBag.Message = "Email n'a pas été envoyé, ...";
                return View(r);
            }
        }
    }
}