﻿//using MyFirstASPAPP.Models;
using MyFirstASPAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstASPAPP.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewWithVariable()
        {
            return View(42);
        }

        public static List<Personne> Personnes
            = new List<Personne>
            {
                new Personne { Id = 1, LastName = "Ly", FirstName = "Khun", Age = 38 },
                new Personne { Id = 2, LastName = "Person", FirstName = "Mike", Age = 38 },
            }; 

        public ActionResult ViewWithModel(int id)
        {
            // rechercher un model en db
            Personne model = Personnes.FirstOrDefault(x => x.Id == id);
            // verifier que ce model soit différent de null
            if(model == null)
            {
                //Declencher une erreur 404
                return new HttpNotFoundResult();
            }
            return View(model);
        }

        public ActionResult ViewWithList()
        {
            return View(Personnes);
        }
    }
}