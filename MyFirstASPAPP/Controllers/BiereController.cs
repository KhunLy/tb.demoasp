﻿using MyFirstASPAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstASPAPP.Controllers
{
    public class BiereController : Controller
    {
        private static List<Biere> bieres = new List<Biere>
        {
            new Biere { Id = 1, Nom = "Orval", Image = "/Content/Images/orval.jpg", Prix = 3 },
            new Biere { Id = 2, Nom = "Jup", Image = "/Content/Images/jup.jpg", Prix = 2 },
            new Biere { Id = 3, Nom = "Cuvée de Trolls", Image = "/Content/Images/cuvee.png", Prix = 2.5m },
        };

        public ActionResult Index()
        {
            return View(bieres);
        }

        public ActionResult Details(int id)
        {
            Biere model = bieres.FirstOrDefault(b => b.Id == id);
            if (model == null) return new HttpNotFoundResult();
            return View(model);
        }

        // affichage du formulaire
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        //traitement du formulaire
        [HttpPost]
        public ActionResult Add(Biere model)
        {
            if (ModelState.IsValid)
            {
                // Ajouter dans la liste statique
                // Plus tard on enregistrera dans la db
                model.Id = bieres.Count + 1;
                bieres.Add(model);
                return RedirectToAction("Index");
            }
            else
            {
                // afficher  un message d'erreur
                return View(model);
            }
            
        }
    }
}