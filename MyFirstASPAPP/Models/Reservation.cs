﻿using MyFirstASPAPP.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFirstASPAPP.Models
{
    public class Reservation
    {
        [Required]
        [MinLength(2, ErrorMessage = "Mon Message Custom")]
        [MaxLength(50)]
        public string NomClient { get; set; }
        
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        public string EmailClient { get; set; }
        
        [Required]
        [Range(1, 50)]
        public int NbPersonnes { get; set; }
        
        [Required]
        [MaxLength(1000)]
        public string Message { get; set; }
        
        [Required]
        [NotBeforeToday]
        public DateTime Date { get; set; }

        [Required]
        [BetweenHours]
        public DateTime Time { get; set; }
    }
}