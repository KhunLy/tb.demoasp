﻿using MyFirstASPAPP.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFirstASPAPP.Models
{
    public class Biere
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Nom { get; set; }

        [Range(1,10000)]
        public decimal Prix { get; set; }

        public string Image { get; set; }
    }
}