﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstASPAPP.Models
{
    public class Personne
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Age { get; set; }
    }
}