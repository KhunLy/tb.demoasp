﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFirstASPAPP.Validators
{
    public class NotBeforeTodayAttribute: ValidationAttribute
    {
        public NotBeforeTodayAttribute()
        {
            ErrorMessage = "La date doit être inférieure à la date du jour";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;
            DateTime date = (DateTime)value;
            return (date > DateTime.Now.AddDays(-1));
        }
    }
}