﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFirstASPAPP.Validators
{
    public class BetweenHoursAttribute: ValidationAttribute
    {
        public BetweenHoursAttribute()
        {
            ErrorMessage = "Veuillez réserver pdt les heures d'ouverture";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;
            DateTime date = (DateTime)value;
            int hour = date.Hour;
            return (hour >= 12 && hour < 14) || (hour >= 18 && hour < 20);
        }
    }
}